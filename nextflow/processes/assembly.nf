process Assembly {

  publishDir "$baseDir/assembly/${sample_id}/"
  maxForks 1
  conda 'dependencies.yaml'

  input:
  tuple val(sample_id), path (reads)

  output:
  path "${reads[0].getSimpleName()}.contigs.fasta",  emit: contigs

  shell:
  """
  megahit -m 0.65 -t 64 -1 !{reads[0]} -2 !{reads[1]} --min-contig-len 200
  ln -s megahit_out/final.contigs.fa !{reads[0].getSimpleName()}.contigs.fasta
  """
}
