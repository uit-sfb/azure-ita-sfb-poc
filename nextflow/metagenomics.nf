#!/usr/bin/env nextflow
nextflow.enable.dsl=2

/*
===================================================================================
                    Marine Metagenomics analysis Pipeline
===================================================================================

nextflow/metagenomics Analysis Pipeline. Started 2020-04-12
#### Homepage / Documentation
-----------------------------------------------------------------------------------
*/

// Help Message
def helpMessage() {
    log.info """
    *********Marine metagenomics analysis*********

    Usage:
    nextflow run metagenomics.nf --reads /data/input/ -with-tower -resume

    Input:
    --reads      path to the directory containing the BGI read file (fastq) (default: $params.reads)
    """
}

if (params.help) exit 0, helpMessage()

//params.reads = "$baseDir/reads_input/"

println """\
         M A R I N E - METAGENOMICS P I P E L I N E
         ==========================================
         reads_illumina   : ${params.illumina}
         reads_bgi        : ${params.bgi}
         outdir_illumina  : ${params.outputill}
         outdir_bgi       : ${params.outputbgi}
         """
         .stripIndent()




// Include modules for each tool
include {Assembly; Assembly as Assembly_bgi} from './processes/assembly.nf'
//include {Mapping; Mapping as Mapping_bgi; Maxbin; Maxbin as Maxbin_bgi; Metabat; Metabat as Metabat_bgi} from './processes/binning.nf'


//Data input
    input_illumina= channel.fromFilePairs( "${params.reads}/*_{fw_clump_bbduk.desensitized,rv_clump_bbduk.desensitized}.fastq.gz", checkIfExists: true)
	input_bgi= channel.fromFilePairs( "${params.reads}/*_{1_fixed,2_fixed}.fq.gz", checkIfExists: true )
				.ifEmpty { exit 1, "Cannot find any reads matching: ${params.reads}" }


//Assembly
workflow assembly_illumina {
  main:
    Assembly(input_illumina)
  emit:
    contig_illumina = Assembly.out
}

workflow assembly_bgi {
  main:
    Assembly_bgi(input_bgi)
  emit:
    contig_bgi = Assembly_bgi.out
}


//Binning
workflow binning_illumina{
    take: contig_illumina
          reads
    main:
      Mapping(contig_illumina, reads)
      Maxbin(contig_illumina, Mapping.out.abun, reads)
      Metabat(contig_illumina, Mapping.out.sort, reads)
}

workflow binning_bgi{
    take: contig_bgi
          reads
    main:
      Mapping_bgi(contig_bgi, reads)
	  Maxbin_bgi(contig_bgi, Mapping.out.abun, reads)
      Metabat_bgi(contig_bgi, Mapping.out.sort, reads)
}


//Final Call
workflow {
    main:
      assembly_illumina()
//      binning_illumina(assembly_illumina.out.contig_illumina, input_illumina)


      assembly_bgi()
//      binning_bgi(assembly_bgi.out.contig_bgi, input_bgi)

}


workflow.onComplete {
	log.info ( workflow.success ? "\nAnalysis Done!\n" : "Oops ... something went wrong" )}


//Done
