# SfB/ITA Azure Cloud PoC
This repository contains all code/information related to the SfB/ITA Azure cloud proof of concept experiments to measure the feasibility of running scientific workloads in the cloud. This repo includes,

 - Ansible-playbooks scripts for deploying software on login and compute nodes.
 - Tokens for starting/stopping compute nodes and Nextflow tower monitoring (encrypted with ansible vault)
 - Scripts for running the Nextflow experiments and how-to's

Contact: Espen Robertsen espen.m.robertsen@uit.no

## deploy/
This section assumes you have access to login and compute nodes and configured your ssh settings correctly. To deploy the software stack, install ansible and run the following command from the deploy/ folder:

    ansible-playbook -i hosts --private-key <path_to_key> deploy.yaml

This downloads and install Miniconda3, sets up and installs an Environment called "Metagenomics" with all needed software for testing, as well as cloning the Nextflow pipelines from this repo. This is done on all VMs you have access to and that are currently switched on.

## Tokens
The tokens file contains tokens and urls needed to start and stop the compute node, as well as running Nextflow with tower for monitoring (https://tower.nf). This file is encrypted with ansible vault. If you need information in this file, contact me and we will most likely give you the password.

To decrypt the tokens file with ansible-vault, type:

    ansible-vault decrypt --vault-password-file <vault_key_file> tokens

All tokens in the tokens file are written as bash environment variables. To configure your current environment, run the command:

    source tokens

You can now start the compute node using the following command:

    curl -d "" -X POST $azure_webhook_url/webhooks?token=$azure_start_token
Or stop the computing node with this command:

    curl -d "" -X POST $azure_webhook_url/webhooks?token=$azure_stop_token

To run Nextflow with Tower for monitoring using the Azure ITA/SfB token, append --with-tower to your Nextflow command like so:

    nextflow metagenomics.nf <parameters> -with-tower

## nextflow/
Experiements are run using the workflow language Nextflow, utilizing its unbuild Conda integration, which creates predefined environments on the fly. Monitoring is available at tower.nf

To run the experiments:

    nextflow run -w /data/work metagenomics.nf --reads /data/input/ -with-tower

Optimally, this should be wrapped in a script running the curl command for shutting off the compute node as mentioned under "Tokens" as the final command, ei.

    # Execute workload
    nextflow run -w /data/work metagenomics.nf --reads /data/input/ -with-tower
    # Wait a bit for good measure
    sleep  120
    # Turn off expensive compute node.
    curl -d "" -X POST $azure_webhook_url/webhooks?token=$azure_stop_token
